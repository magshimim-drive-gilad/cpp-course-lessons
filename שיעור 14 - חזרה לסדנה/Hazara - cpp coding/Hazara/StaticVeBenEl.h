#pragma once
class StaticVeBenEl
{
	public:
		StaticVeBenEl(int x, int y);
		int add();
		static int addSt(int x, int y);
		static int getIndex();
		
	private:
		int _x;
		int _y;
		static int _index;
		
};

