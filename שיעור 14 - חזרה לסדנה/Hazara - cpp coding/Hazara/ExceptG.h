#pragma once
#include <exception>
#include <string>

class ExceptG : public std::exception
{
	public:
		ExceptG(std::string str);
		virtual const char* what() const;
	
	private:
		std::string _str;
};

