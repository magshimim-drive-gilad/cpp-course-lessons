#include <iostream>
#include <string>
#include "ExceptG.h"

void print(int num)
{
	if (num < 0)
	{
		throw (ExceptG("negative number"));
	}
	if (num == 0)
	{
		throw (num);
	}
	std::cout << "Here is: " << num << std::endl;

}

int main()
{
	try
	{
		print(-1);
		print(0);
	}
	catch (const std::string& str)
	{
		std::cerr << str << std::endl;
	}
	catch (const int& num)
	{
		std::cerr << num << std::endl;
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}

	system("pause");
	return (0);
}