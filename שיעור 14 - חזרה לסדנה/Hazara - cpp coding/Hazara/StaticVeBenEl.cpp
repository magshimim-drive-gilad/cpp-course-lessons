#include "StaticVeBenEl.h"


int StaticVeBenEl::_index = 0;

StaticVeBenEl::StaticVeBenEl(int x, int y) : _x(x), _y(y)
{
	_index++;
}

int StaticVeBenEl::add()
{
	return (_x + _y);
}

int StaticVeBenEl::addSt(int x, int y)
{
	return (x + y);
}

int StaticVeBenEl::getIndex()
{
	return _index;
}
